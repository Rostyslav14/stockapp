/** @type {import('next').NextConfig} */
const nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
  images: {
    remotePatterns: [
      {
        protocol: 'https',
        hostname: 'www.bloomberg.com',
        port: '3000',
        pathname: '/news/**',
      },
      {
        protocol: 'https',
        hostname: 'data.bloomberglp.com',
        port: '3000',
        pathname: '/company/**',
      },
      {
        protocol: 'https',
        hostname: 'image.cnbcfm.com',
        port: '3000',
        pathname: '/api/**',
      },
      {
        protocol: 'https',
        hostname: 'images.mktw.net',
        port: '3000',
        pathname: '/**',
      },
      {
        protocol: 'https',
        hostname: 'mw3.wsj.net',
        port: '3000',
        pathname: '/**'
      },
      {
        protocol: 'https',
        hostname: 'sc.cnbcfm.com',
        port: '3000',
        pathname: '/**'
      },
      {
        protocol: 'https',
        hostname: 'static.finnhub.io',
        port: '',
        pathname: '/**',
      },
      {
        protocol: 'https',
        hostname: 'static2.finnhub.io',
        port: '',
        pathname: '/**',
      },
      
    ],
  },
  compiler: {
    styledComponents: true
  }
};

module.exports = nextConfig;
