export interface ITableData {
  fromDate: string;
  toDate: string;
  eventDescription: string;
  url: string;
}

export type TOrder = 'asc' | 'desc';

export interface ICompanyInfo {
  country: string;
  currency: string;
  exchange: string;
  finnhubIndustry: string;
  ipo: string;
  logo: string;
  marketCapitalization: number;
  name: string;
  phone: string;
  shareOutstanding: number;
  ticker: string;
  weburl: string;
}
