export interface ICryptoCurrency {
  name?: string;
  c: number;
  d: number;
  dp: number;
  h: number;
  l: number;
  o: number;
  pc: number;
  t: number;
}

export interface ISearchItem {
  description: string;
  displaySymbol: string;
  symbol: string;
  type: string;
}

export interface IStockInfo {
  address: string;
  city: string;
  country: string;
  currency: string;
  cusip: string;
  sedol: string;
  description: string;
  employeeTotal: string;
  exchange: string;
  ggroup: string;
  gind: string;
  gsector: string;
  gsubind: string;
  ipo: string;
  isin: string;
  marketCapitalization: number;
  naics: string;
  naicsNationalIndustry: string;
  naicsSector: string;
  naicsSubsector: string;
  name: string;
  phone: string;
  shareOutstanding: number;
  state: string;
  ticker: string;
  weburl: string;
  logo: string;
  finnhubIndustry: string;
}

export interface ChartInfo<T> {
  s: string;
  v: T;
  c: T;
  h: T;
  l: T;
  o: T;
}

export interface ICurrencyInfo {
  country: string;
  currency: string;
  exchange: string;
  finnhubIndustry: string;
  ipo: string;
  logo: string;
  marketCapitalization: number;
  name: string;
  phone: string;
  shareOutstanding: number;
  ticker: string;
  weburl: string;
}
