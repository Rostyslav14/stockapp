export const PREVIEW_CURRENCIES = [
  'AAPL',
  'DELL',
  'COUP',
  'WDC',
  'HPE',
  'MANU',
] as const;
export const TIME_CATEGORIES = ['Today', '1W', '1M', '6M', '1Y'] as const;
export const PEERS_CATEGORIES = ['sector', 'industry', 'subIndustry'] as const;
export const NEWS_CATEGORIES = ['forex', 'crypto', 'merger'];
