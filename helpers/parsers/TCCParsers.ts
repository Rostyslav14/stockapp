export function createData(
  name: string,
  calories: number,
  fat: number,
  carbs: number,
  protein: number
): any {
  return {
    name,
    calories,
    fat,
    carbs,
    protein,
  };
}
