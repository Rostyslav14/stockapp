import { ICryptoCurrency } from '../types/crypto';

export function getUniqueCryptos(arr: ICryptoCurrency[]) {
  const res = Array.from(
    new Map(arr.map((item: ICryptoCurrency) => [item.s, item])).values()
  );
  return res;
}
// const unixToDate = (string) => {
//     const unixTime = string;
//     const date = new Date(unixTime * 1000);
//     const newText = date.toLocaleDateString("en-US");

//     return newText
//   };
export function parseChartResponse(arr1: number[], arr2: number[]) {
  const arr = [];
  for (let i = 0; i < arr1.length; i++) {
    const localObj = { x: `${arr2[i]}`, y: arr1[i] };
    arr.push(localObj);
  }
  return arr;
}
