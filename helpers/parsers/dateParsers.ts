export function chartDateParser(arg: string) {
  //only parse date for chart
  const now = Math.floor(new Date().getTime() / 1000);
  const start = new Date();
  switch (arg) {
    case 'Today': {
      start.setHours(0, 0, 0, 0);
      const from = Math.floor(start.getTime() / 1000 - 43200);
      return { from: from.toString(), to: now.toString(), resolution: '5' };
    }
    case '1W': {
      start.setMonth(start.getDay() - 7);
      start.setHours(0, 0, 0, 0);
      const from = Math.floor(start.getTime() / 1000);
      return { from: from.toString(), to: now.toString(), resolution: '15' };
    }
    case '1M': {
      start.setMonth(start.getMonth() - 1);
      start.setHours(0, 0, 0, 0);
      const from = Math.floor(start.getTime() / 1000);
      return { from: from.toString(), to: now.toString(), resolution: 'D' };
    }
    case '6M': {
      start.setMonth(start.getMonth() - 6);
      start.setHours(0, 0, 0, 0);
      const from = Math.floor(start.getTime() / 1000);
      return { from: from.toString(), to: now.toString(), resolution: 'D' };
    }
    case '1Y': {
      start.setFullYear(start.getFullYear() - 1);
      start.setHours(0, 0, 0, 0);
      const from = Math.floor(start.getTime() / 1000);
      return { from: from.toString(), to: now.toString(), resolution: 'D' };
    }
  }
}

export function convertDateToUNIXTS(userDate: string) {
  const [dateValues, timeValues] = userDate.split(' ');
  const [month, day, year] = dateValues.split('-');
  const [hours, minutes, seconds] = timeValues.split(':');
  const date = new Date(+year, month - 1, +day, +hours, +minutes, +seconds);
  const timestampInMs = date.getTime();
  const timestampInSeconds = Math.floor(date.getTime() / 1000);
  return timestampInSeconds;
}

export function parseDate(date: string) {
  // INPUT date format example = '1980-02-02'
  //OUTPUT EXAMPLE = 'Dec 12, 1980'
  const newDate = new Date(date);
  const normalizedDate = new Date(
    newDate.getTime() - newDate.getTimezoneOffset() * -60000
  );
  const formattedDate = normalizedDate.toLocaleDateString('en-US', {
    day: 'numeric',
    month: 'short',
    year: 'numeric',
  });
  return formattedDate;
}
