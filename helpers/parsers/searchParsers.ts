export function parseCountryFromSymbol(str: string) {
  if (str.includes('.')) return str.slice(str.lastIndexOf('.') + 1, str.length);
  return 'US';
}
