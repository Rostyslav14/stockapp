import { FC, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { IRootState } from 'redux/reducers/rootReducer';
import { NEWS_CATEGORIES } from 'helpers/constants/constants';
import styled from 'styled-components';
import { INews } from 'helpers/types/news';
import { newsService } from 'services/news.service';
import { controlsActions } from 'redux/actions/controlsActions';
import { useRouter } from 'next/router';
import { NewsColumn } from '@/components/screens/News/NewsColumn/NewsColumn';
import { LoadingSpin } from '@/components/shared/LoadingSpin/LoadingSpin';
import { BottomBlock } from '@/components/screens/News/BottomBlock/BottomBlock';
import { Button } from '@/components/shared/BasicButton/Button';

const Container = styled.div<{ isMultipleColumns: boolean }>`
  position: relative;
  display: grid;
  grid-template-columns: ${props =>
    props.isMultipleColumns ? '1fr 1fr 1fr' : '1fr'};
  column-gap: 15px;
  width: 100%;
  padding-bottom: 25px;
`;
const PositionedButton = styled(Button)`
  position: absolute;
  left: 50%;
  bottom: 40px;
  -webkit-transform: translateX(-50%);
  transform: translateX(-50%);
`

interface IProps {
  isLoaderShown: boolean;
  dispatch: any;
}

interface INewsCategory {
  name: string;
  list: INews[];
}

export const News: FC<IProps> = props => {
  const { isLoaderShown, dispatch } = props;
  const [newsByCategories, setNewsByCategories] = useState<INewsCategory[]>([]);
  const [isFullListShown, setIsFullListShown] = useState<boolean>(false);
  const [isMultipleColumns, setIsMultipleColumns] = useState<boolean>(true);
  const router = useRouter();
  const category = router.query.category as string;

  useEffect(() => {
    dispatch(controlsActions.showLoader());
    if (category === 'all') {
      setIsMultipleColumns(true);
      const allPromises: Promise<any>[] = [];
      for (const el of NEWS_CATEGORIES) {
        allPromises.push(newsService.getNews(el));
      }
      Promise.all(allPromises)
        .then(data =>
          data.map((el, i) => ({
            name: NEWS_CATEGORIES[i],
            list: el.data.slice(0, 10),
          }))
        )
        .then(data => setNewsByCategories(data))
        .catch(err => console.error(err))
        .finally(() => dispatch(controlsActions.hideLoader()));
    } else if (NEWS_CATEGORIES.includes(category)) {
      setIsMultipleColumns(false);
      newsService
        .getNews(category)
        .then(data => data.data)
        .then(data =>
          setNewsByCategories([{ name: category, list: data.slice(0, 10) }])
        )
        .catch(err => console.error(err))
        .finally(() => dispatch(controlsActions.hideLoader()));
    }
  }, [category, dispatch]);

  const handleComlumnHeaderClick = (headerName: string) => router.push(headerName);

  const ToggleFullListShown = () => setIsFullListShown(prev => !prev);
  
  const onNewsClick = (link:string) => dispatch(controlsActions.activateRedirectDialog(link))

  return (
    <>
      {isLoaderShown ? (
        <LoadingSpin />
      ) : (
        <Container isMultipleColumns={isMultipleColumns}>
          {newsByCategories.map(category => (
            <NewsColumn
              isFullImg={!isMultipleColumns}
              isFullList={isFullListShown}
              categoryList={category}
              onHeaderClick={handleComlumnHeaderClick}
              onNewsClick={onNewsClick}
              key={category.name}
            />
          ))}

          <BottomBlock
            onClick={ToggleFullListShown}
            isFullListShown={isFullListShown}
          >
            {isFullListShown ? ' hide extra news ' : <PositionedButton>More</PositionedButton>}
          </BottomBlock>
        </Container>
      )}
    </>
  );
};

function mapStateToProps(state: IRootState) {
  return {
    isLoaderShown: state.controls.isLoaderShown,
  };
}
export default connect(mapStateToProps)(News);
