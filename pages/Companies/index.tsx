import { FC, useEffect, useState } from 'react';
import styled from 'styled-components';
import { connect } from 'react-redux';
import { stockService } from 'services/stock.service';
import { IRootState } from '../../redux/reducers/rootReducer';
import { LoadingSpin } from '@/components/shared/LoadingSpin/LoadingSpin';
import { SearchBar } from '@/components/shared/SearchBar/SearchBar';
import { PREVIEW_CURRENCIES } from 'helpers/constants/constants';
import { ICompanyInfo } from 'helpers/types/others';
import { controlsActions } from 'redux/actions/controlsActions';
import { CompnayItemsList } from '@/components/screens/Companies/CompanyItemsList/CompanyItemsList';
import useDebouncedCallback from 'helpers/hooks/useDebouncedCallback';
import { useRouter } from 'next/router';

interface IProps {
  dispatch: any;
  isLoaderShown:boolean;
}

const Container = styled.div``;
export const Companies: FC<IProps> = props => {
  const [companiesInfo, setCompaniesInfo] = useState<ICompanyInfo[]>();
  const [inputValue,setInputValue] = useState('')
  const router = useRouter()
  const { isLoaderShown } = props;
  const { dispatch } = props;

  const debouncedChangeHandler = useDebouncedCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      router.push(`Search/${e.target.value.replace(/\s/g, '')}`);
    },
    400
  );

  
  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.currentTarget.value);
    debouncedChangeHandler(e);
  };

  useEffect(() => {
    const allPromises = [];
    dispatch(controlsActions.showLoader());
    for (const el of PREVIEW_CURRENCIES) {
      allPromises.push(stockService.getCompanyInfo(el).then(data => data.data));
    }
    Promise.all(allPromises)
      .then(data => setCompaniesInfo(data))
      .catch(err => console.log(err))
      .finally(() => dispatch(controlsActions.hideLoader()));
  }, []);
  const handleCompanyItemClick = (link:string) =>{
    dispatch(controlsActions.activateRedirectDialog(link))
  }

  return (
    <>
      {isLoaderShown ? <LoadingSpin /> : 
      <Container>
        <SearchBar value={inputValue} onChange={handleInput} />
        {companiesInfo && <CompnayItemsList onItemWebsiteClick={handleCompanyItemClick} companiesList={companiesInfo} />}
      </Container>}
    </>
  );
};

function mapStateToProps(state: IRootState) {
  return {
    isLoaderShown: state.controls.isLoaderShown,
  };
}
export default connect(mapStateToProps)(Companies);
