import { ICryptoCurrency, ICurrencyInfo } from 'helpers/types/crypto';
import Error from 'next/error';

interface ILocalStore {
  chartDates: {
    from: string;
    to: string;
    resolution: string;
  };
  chartData: { x: string; y: number }[];
  currencyValue: ICryptoCurrency | null;
  timeStamp: {
    name: string;
    index: number;
  };
  currencyInfo: ICurrencyInfo | null;
  peersGroupingBy: string;
  peersNames: string[];
  peersData: ICryptoCurrency[];
  error:Error | null
}
const data: ILocalStore = {
  chartDates: {
    from: '',
    to: '',
    resolution: '5',
  },
  chartData: [],
  currencyValue: null,
  timeStamp: {
    name: 'Today',
    index: 0,
  },
  currencyInfo: null,
  peersGroupingBy: 'sector',
  peersNames: [],
  peersData: [],
  error:null
};

const reducerCallback = (state: ILocalStore, action: any) => {
  switch (action.type) {
    case 'setChartDates':
      return {
        ...state,
        chartDates: action.payload.data,
      };
    case 'setChartData':
      return {
        ...state,
        chartData: action.payload.data,
      };
    case 'setCurrencyValue':
      return {
        ...state,
        currencyValue: action.payload.data,
      };
    case 'setTimeStamp':
      return {
        ...state,
        timeStamp: {
          name: action.payload.name,
          index: action.payload.index,
        },
      };
    case 'setCurrencyInfo':
      return {
        ...state,
        currencyInfo: action.payload.data,
      };
    case 'setPeersGroupingBy':
      return {
        ...state,
        peersGroupingBy: action.payload.category,
      };
    case 'setPeersNames':
      return {
        ...state,
        peersNames: action.payload.names,
      };
    case 'setPeersData':
      // console.log(action.payload)
      return {
        ...state,
        peersData: action.payload.data,
      };
    case 'setError':
      return {
        ...state,
        error:action.payload.error
      }
    default:
      return state;
  }
};

export { data, reducerCallback };
