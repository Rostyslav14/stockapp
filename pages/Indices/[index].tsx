import { FC, useEffect, useMemo, useReducer } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import { stockService } from 'services/stock.service';
import { parseChartResponse } from 'helpers/parsers/cryptoParsers';
import { chartDateParser } from '../../helpers/parsers/dateParsers';
import { NEWS_CATEGORIES } from 'helpers/constants/constants';
import { IRootState } from 'redux/reducers/rootReducer';
import { controlsActions } from 'redux/actions/controlsActions';
import { BuyingPower } from '@/components/screens/Indicies/BuyingPower/BuyingPower';
import { News } from '@/components/screens/Indicies/News/News';
import { LoadingSpin } from '@/components/shared/LoadingSpin/LoadingSpin';
import { Peers } from '@/components/screens/Indicies/Peers/Peers';
import { CurrencyPreviewList } from '@/components/shared/CurrencyPreviewList/CurrencyPreviewList';
import { Graphick } from '@/components/screens/Indicies/Graphick/Graphick';
import { LineChart } from '@/components/screens/Indicies/Chart/Chart';
import { reducerCallback } from './indiciesStore';
import { data } from './indiciesStore';
import ErrorPlug from '@/components/shared/403ErrorPlug/403ErrorPlug';

interface IProps {
  isLoaderShown: boolean;
  dispatch: any;
}

const Indicies: FC<IProps> = props => {
  const { isLoaderShown, dispatch } = props;
  const router = useRouter();
  const id = router.query.index as string;
  const [database, localDispatch] = useReducer(reducerCallback, data);

  useMemo(() => {
    const data = chartDateParser(database.timeStamp.name);
    localDispatch({ type: 'setChartDates', payload: { data: data } });
  }, [database.timeStamp]);

  useEffect(() => {
    stockService
      .getSimilarPeerNames(id, database.peersGroupingBy)
      .then(data => data.data.slice(0, 7))
      .then(data =>
        localDispatch({ type: 'setPeersNames', payload: { names: data } })
      );
  }, [id, database.peersGroupingBy]);

  useEffect(() => {
    if (id) {
      dispatch(controlsActions.showLoader());

      stockService
        .getCurrencyData(id)
        .then(data => data.data)
        .then(data =>
          localDispatch({ type: 'setCurrencyValue', payload: { data } })
        )
        .catch(err => console.log(err));
      stockService
        .getStockData(id)
        .then(data => {
          localDispatch({
            type: 'setCurrencyInfo',
            payload: { data: data.data },
          });
        })
        .catch(err => console.log(err));
      stockService
        .getChartData(
          id,
          database.chartDates.resolution,
          database.chartDates.from,
          database.chartDates.to
        )
        .then(data => parseChartResponse(data?.data?.c, data?.data?.t))
        .then(data => {
          localDispatch({ type: 'setChartData', payload: { data } });
        })
        .catch(err => {
          localDispatch({ type: 'setError', payload: { error: err } });
        })
        .finally(() => dispatch(controlsActions.hideLoader()));
    }
  }, [database.timeStamp, id, database.chartDates, dispatch]);

  useEffect(() => {
    const allReq = [];
    if (!!database.peersNames.length) {
      for (const el of database.peersNames) {
        allReq.push(stockService.getCurrencyData(el).then(data => data.data));
      }
      Promise.all(allReq)
        .then(data => {
          localDispatch({ type: 'setPeersData', payload: { data } });
        })
        .catch(e => {
          console.log(e);
        });
    }
  }, [database.peersNames]);

  useMemo(() => {
    for (let i = 0; i < database.peersData.length; i++) {
      database.peersData[i].name = database.peersNames[i];
    }
  }, [database.peersNames, database.peersData]);

  function handleActiveTimeStamp(event: any, key: number) {
    localDispatch({
      type: 'setTimeStamp',
      payload: { name: event.target.textContent, index: key },
    });
  }
  function handleSelect(e: any) {
    localDispatch({
      type: 'setPeersGroupingBy',
      payload: { category: e.target.value },
    });
  }
  function handleNewsCategoryClick(itemName: string) {
    router.push(`/News/${itemName}`);
  }

  if (isLoaderShown) {
    return <LoadingSpin />;
  }
  
  if (database?.error?.response?.status === 403) {
    return <ErrorPlug />;
  }

  return (
    <>
      <div>
        {!!database.currencyInfo && !!database.currencyValue && (
          <Graphick
            currency={database.currencyValue}
            info={database.currencyInfo}
            activeIndex={database.timeStamp.index}
            handleTimeStampClick={handleActiveTimeStamp}
          >
            <LineChart
              name={id}
              data={database.chartData as Array<{ x: string; y: number }>}
            />
          </Graphick>
        )}

        <BuyingPower />

        {!!database.peersData.length && (
          <Peers value={database.peersGroupingBy} onChangeSelect={handleSelect}>
            <CurrencyPreviewList currencyList={database.peersData} />
          </Peers>
        )}
        <News
          onItemClick={handleNewsCategoryClick}
          categories={NEWS_CATEGORIES}
        />
      </div>
    </>
  );
};

function mapStateToProps(state: IRootState) {
  return {
    isLoaderShown: state.controls.isLoaderShown,
  };
}
export default connect(mapStateToProps)(Indicies);
