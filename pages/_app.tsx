import React from 'react';
import { Provider } from 'react-redux';
import { MainLayout } from 'components/layout';
import { store } from 'redux/store/store';
import 'styles/globals.scss';
import 'styles/customGlobals.scss';
import Head from 'next/head';

export default function App({ Component, pageProps }) {
  return (
    <Provider store={store}>
      <Head>
        <link rel="shortcut icon" href="binance.ico" />
        <title>Trading app</title>
      </Head>
      <MainLayout>
        <Component {...pageProps} />
      </MainLayout>
    </Provider>
  );
}
