import { FC } from 'react';
import Link from 'next/link';
import Image from 'next/image';

interface IProps {}

const FourOnFour: FC<IProps> = props => {
  return (
    <div
      style={{
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        height: '100vh',
        flexDirection: 'column',
      }}
    >
      <div
        style={{
          marginBottom: '200px',
          display: 'flex',
          flexDirection: 'column',
          justifyContent: 'center',
          alignItems: 'center',
        }}
      >
        <div style={{ width: '340px', height: '205px', position: 'relative' }}>
          <Image fill alt="Not Found" src={'/images/404-error.png'}></Image>
        </div>
        <div
          style={{
            marginTop: '65px',
            display: 'flex',
            alignItems: 'center',
            fontSize: '14px',
            lineHeight: '20px',
          }}
        >
          <span
            style={{ fontSize: '24px', lineHeight: '28px', fontWeight: '500' }}
          >
            Sorry! The page you’re looking for cannot be found.{' '}
          </span>
          <Link
            style={{ marginLeft: '16px', color: 'var(--colors-link-hv)' }}
            href="/"
          >
            {' '}
            Go to Homepage
          </Link>
        </div>
      </div>
    </div>
  );
};

export default FourOnFour;
