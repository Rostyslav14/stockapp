import React, { useEffect, useMemo, useState, FC } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import { PREVIEW_CURRENCIES } from 'helpers/constants/constants';
import { INews } from 'helpers/types/news';
import { ICryptoCurrency } from 'helpers/types/crypto';
import useDebouncedCallback from 'helpers/hooks/useDebouncedCallback';
import { stockService } from 'services/stock.service';
import { newsService } from 'services/news.service';
import NewsCardList from '@/components/screens/Home/NewsCardList/NewsCardList';
import { SearchBar } from 'components/shared/SearchBar/SearchBar';
import { CurrencyPreviewList } from '@/components/shared/CurrencyPreviewList/CurrencyPreviewList';
import { LoadingSpin } from '@/components/shared/LoadingSpin/LoadingSpin';
import { controlsActions } from 'redux/actions/controlsActions';
import { IRootState } from 'redux/reducers/rootReducer';

interface IProps {
  isLoaderShown: boolean;
  dispatch: any;
}

const Index: FC<IProps> = props => {
  const { isLoaderShown, dispatch } = props;
  const router = useRouter();
  const [lastUpdated, setLastUpdated] = useState<ICryptoCurrency[]>([]);
  const [news, setNews] = useState<INews[]>([]);
  const [inputValue, setInputValue] = useState<string>('');

  const debouncedChangeHandler = useDebouncedCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      router.push(`Search/${e.target.value.replace(/\s/g, '')}`);
    },
    400
  );

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.currentTarget.value);
    debouncedChangeHandler(e);
  };

  useMemo(() => {
    for (let i = 0; i < lastUpdated.length; i++) {
      lastUpdated[i].name = PREVIEW_CURRENCIES[i];
    }
  }, [lastUpdated]);

  useEffect(() => {
    dispatch(controlsActions.showLoader());
    const allReq = [];
    for (const el of PREVIEW_CURRENCIES) {
      allReq.push(stockService.getCurrencyData(el).then(data => data.data));
    }
    Promise.all(allReq)
      .then(data => {
        setLastUpdated(data);
      })
      .catch(e => {
        console.log(e);
      });

    newsService
      .getNews()
      .then(data => setNews(data.data.slice(0, 24)))
      .catch(err => console.log(err))
      .finally(() => dispatch(controlsActions.hideLoader()));
  }, []);

  const onNewsClick = (link:string)=>{
    dispatch(controlsActions.activateRedirectDialog(link))
  }

  return (
    <>
      {isLoaderShown ? (
        <LoadingSpin />
      ) : (
        <>
          <CurrencyPreviewList currencyList={lastUpdated} />
          <SearchBar onChange={handleInput} value={inputValue} />
          {news[0] && <NewsCardList onNewsClick={onNewsClick} news={news} />}
        </>
      )}
    </>
  );
};

function mapStateToProps(state: IRootState) {
  return {
    isLoaderShown: state.controls.isLoaderShown,
  };
}
export default connect(mapStateToProps)(Index);
