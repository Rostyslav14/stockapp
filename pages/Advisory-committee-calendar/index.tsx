import { FC, useEffect, useState } from 'react';
import { stockService } from 'services/stock.service';
import { ITableData } from 'helpers/types/others';
import { convertDateToUNIXTS } from 'helpers/parsers/dateParsers';
import { useAppDispatch, useAppSelector } from 'helpers/hooks/typedStoreHooks';
import { ACCTable } from '@/components/screens/ACC/Table/Table';
import { Description } from '@/components/screens/ACC/Description/Description';
import { LoadingSpin } from '@/components/shared/LoadingSpin/LoadingSpin';
import { controlsActions } from 'redux/actions/controlsActions';
interface IProps {}

type Order = 'asc' | 'desc';

const ACC: FC<IProps> = props => {
  const isLoaderShown = useAppSelector(store => store.controls.isLoaderShown);
  const dispatch = useAppDispatch();
  const [order, setOrder] = useState<Order>('asc');
  const [orderBy, setOrderBy] = useState<'fromDate' | 'toDate'>('fromDate');
  const [page, setPage] = useState(0);
  const [rowsPerPage, setRowsPerPage] = useState(20);
  const [ACCdata, setACCData] = useState<ITableData[]>([]);

  useEffect(() => {
    dispatch(controlsActions.showLoader());
    stockService
      .getACCData()
      .then(data => setACCData(data.data))
      .catch(err => console.error(err))
      .finally(() => dispatch(controlsActions.hideLoader()));
  }, []);

  const handleRequestSort = (
    event: React.MouseEvent<unknown>,
    property: 'fromDate' | 'toDate'
  ) => {
    const isAsc = orderBy === property && order === 'asc';
    setOrder(isAsc ? 'desc' : 'asc');
    setOrderBy(property);
    let sorted: ITableData[] = [];
    if (isAsc) {
      sorted = ACCdata.sort(
        (a, b) =>
          convertDateToUNIXTS(b[property]) - convertDateToUNIXTS(a[property])
      );
    } else {
      sorted = ACCdata.sort(
        (a, b) =>
          convertDateToUNIXTS(a[property]) - convertDateToUNIXTS(b[property])
      );
    }
    setACCData(sorted);
  };

  const handleChangePage = (event: unknown, newPage: number) => {
    setPage(newPage);
  };

  const handleDescriptionLink = (link:string)=>dispatch(controlsActions.activateRedirectDialog(link))

  return (
    <>
      {isLoaderShown ? (
        <LoadingSpin />
      ) : (
        <>
          <Description handleLinkClick={handleDescriptionLink} />
          <ACCTable
            rows={ACCdata}
            order={order}
            orderBy={orderBy}
            page={page}
            rowsPerPage={rowsPerPage}
            handleChangePage={handleChangePage}
            // handleChangeRowsPerPage={handleChangeRowsPerPage}
            handleRequestSort={handleRequestSort}
          />
        </>
      )}
    </>
  );
};

export default ACC;
