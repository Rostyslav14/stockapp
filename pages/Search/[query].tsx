import React, { FC, useEffect, useState } from 'react';
import { connect } from 'react-redux';
import { useRouter } from 'next/router';
import styled from 'styled-components';
import { stockService } from 'services/stock.service';
import { ISearchItem } from 'helpers/types/crypto';
import useDebouncedCallback from 'helpers/hooks/useDebouncedCallback';
import { SearchItemsList } from '@/components/screens/Search/SeatchItemsList/SearchItemsList';
import { SearchBar } from 'components/shared/SearchBar/SearchBar';
import { LoadingSpin } from '@/components/shared/LoadingSpin/LoadingSpin';
import { controlsActions } from 'redux/actions/controlsActions';

interface IProps {
  query?: string;
  isLoaderShown: boolean;
  dispatch: any;
}

const Title = styled.h1`
  margin: 0;
  padding: 0;
  color: var(--colors-yellow-main);
  font-size: 32px;
`;

const OptionalTitle = styled.h2`
  font-size: 42px;
  font-weight: 600;
`;
const Search: FC<IProps> = props => {
  const { isLoaderShown, dispatch } = props;
  const router = useRouter();
  const value = router.query.query;
  const [searchedItems, setSearchedItems] = useState<ISearchItem[]>([]);
  const [inputValue, setInputValue] = useState<string>(value as string);

  useEffect(() => {
    stockService
      .searchBySymbol(value as string)
      .then(data => setSearchedItems(data.data.result));
  }, []);

  useEffect(() => {
    dispatch(controlsActions.showLoader());
    stockService
      .searchBySymbol(inputValue)
      .then(data => setSearchedItems(data.data.result.slice(0,50)))
      .catch(err => console.log(err))
      .finally(() => dispatch(controlsActions.hideLoader()));
  }, [router]);

  const debouncedChangeHandler = useDebouncedCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      router.push(`${e.target.value.replace(/\s/g, '')}`);
    },
    400
  );

  const handleInput = (e: React.ChangeEvent<HTMLInputElement>) => {
    setInputValue(e.currentTarget.value);
    debouncedChangeHandler(e);
  };

  return (
    <>
      {isLoaderShown ? (
        <LoadingSpin />
      ) : (
        <>
          {SearchItemsList.length > 0 && (
            <Title>
              Count:
              {searchedItems.length}
            </Title>
          )}
          <SearchBar onChange={handleInput} value={inputValue} />
          {searchedItems.length ? (
            <SearchItemsList searchItems={searchedItems} />
          ) : (
            <OptionalTitle>
              We did not find anything for your request.
            </OptionalTitle>
          )}
        </>
      )}
    </>
  );
};
function mapStateToProps(state: any) {
  return {
    isLoaderShown: state.controls.isLoaderShown,
  };
}

export default connect(mapStateToProps)(Search);
