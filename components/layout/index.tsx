import { FC, ReactNode } from 'react';
import styled from 'styled-components';
import { Container } from './Container/Container';
import { Header } from './Header/Header';
import { Footer } from './Footer/Footer';
import { RedirectDialog } from './RedirectDialog/RedirectDialog';

interface IProps {
  children: ReactNode;
}
const Main = styled.main`
  padding-top: 130px;
  padding-bottom: 15px;
  min-height: 75vh;
`;

export const MainLayout: FC<IProps> = props => {
  const { children } = props;
  return (
    <>
      <Header />
      <Main>
        <Container>{children}</Container>
      </Main>
      <RedirectDialog/>
      <Footer />
    </>
  );
};

