import { FC, useEffect, useState } from 'react';
import { IoMoon, IoMoonOutline } from 'react-icons/io5';
import Link from 'next/link';
import styled from 'styled-components';
import { Container } from '../Container/Container';
import { NavItem } from './NavItem/NavItem';
const HeaderBox = styled.header`
  height: 115px;
  width: 100%;
  position: fixed;
  box-shadow: var(--shadow);
  z-index: 10;
`;
const Wrapper = styled.div`
  background-color: var(--colors-bg);
  position: absolute;
  display: flex;
  align-items: center;
  justify-content: center;
  top: 0;
  right: 0;
  left: 0;
  height: 100%;
`;
const Nav = styled.nav`
  display: flex;
  align-items: center;
  justify-content: center;
`;

const NavList = styled.ul`
  display: flex;
  gap: 30px;
  align-items: center;
  justify-content: center;
`;

const LogoWrapper = styled.div`
  position: relative;
  min-width: 320px;
  min-height: 75px;
  transition: box-shadow 0.5s;
  &:hover {
    box-shadow: var(--shadow-long);
    transition: box-shadow 0.5s;
  }
  background-image: url('/images/icons/Binance-Logo.wine.png');
  background-repeat: no-repeat;
  background-size: contain;
`;

const ThemeLogoWrapper = styled.div`
  cursor: pointer;
  margin-left: 50px;
  text-align: center;
  display: flex;
  gap: 10px;
  align-items: center;
`;

interface IProps {}

export const Header: FC<IProps> = props => {
  const [theme, setTheme] = useState<'light' | 'dark'>('dark');
  useEffect(() => {
    document.body.setAttribute('data-theme', theme);
  }, [theme]);

  const handleTheme = () => {
    setTheme(theme === 'light' ? 'dark' : 'light');
  };
  return (
    <HeaderBox>
      <Container>
        <Wrapper>
          <Nav>
            <NavList>
              <NavItem text="News" to="/News/all" />
              <NavItem text="Companies" to="/Companies" />
              <Link href="/">
                <LogoWrapper />
              </Link>
              <NavItem text="Trade" to="/Trade" />
              <NavItem text="ACC" to="/Advisory-committee-calendar" />
            </NavList>
            <ThemeLogoWrapper onClick={handleTheme}>
              {theme === 'dark' ? <IoMoonOutline /> : <IoMoon />}
              {theme === 'dark' ? 'Light Theme' : 'Dark Theme'}
            </ThemeLogoWrapper>
          </Nav>
        </Wrapper>
      </Container>
    </HeaderBox>
  );
};
