import { FC } from 'react';
import Link from 'next/link';
import styled from 'styled-components';

interface IProps {
  to: string;
  text: string;
}
const Li = styled.li`
  font-size: 24px;
  text-transform: uppercase;
  letter-spacing: 1px;
  font-weight: 700;
`;

const StyledA = styled(Link)`
  z-index: 10;
  color: var(--colors-text);
  text-decoration-line: none;
  transition: color 0.3s;
  font-size: 24px;
  text-transform: uppercase;
  letter-spacing: 1px;
  font-weight: 700;

  &:hover,
  &:focus {
    color: var(--color-yellow-main);
    border-bottom: 2px solid var(--color-yellow-main);
  }
  &:active {
    color: var(--colors-text);
  }
`;

export const NavItem: FC<IProps> = props => (
  <Li>
    <StyledA href={props.to}>{props.text}</StyledA>
  </Li>
);

