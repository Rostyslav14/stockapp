import { FC } from 'react';
import Button from '@mui/material/Button';
import Dialog from '@mui/material/Dialog';
import DialogActions from '@mui/material/DialogActions';
import DialogContent from '@mui/material/DialogContent';
import DialogContentText from '@mui/material/DialogContentText';
import DialogTitle from '@mui/material/DialogTitle';
import useMediaQuery from '@mui/material/useMediaQuery';
import { useTheme } from '@mui/material/styles';
import { useAppDispatch, useAppSelector } from 'helpers/hooks/typedStoreHooks';
import { controlsActions } from 'redux/actions/controlsActions';

interface IProps {}

export const RedirectDialog: FC<IProps> = props => {
  const isRedirectDialogShown = useAppSelector(
    data => data.controls.isRedirectDialogShown
  );
  const redirectLink = useAppSelector(store => store.controls.redirectLink);
  const dispatch = useAppDispatch();
  const theme = useTheme();
  const fullScreen = useMediaQuery(theme.breakpoints.down('md'));

  const handleClose = () => {
    dispatch(controlsActions.deactivateRedirectDialog());
  };

  return (
    <div>
      <Dialog
        fullScreen={fullScreen}
        open={isRedirectDialogShown}
        onClose={handleClose}
        aria-labelledby="responsive-dialog-title"
      >
        <DialogTitle id="responsive-dialog-title">
          Go to another site?
        </DialogTitle>
        <DialogContent>
          <DialogContentText>
            You will be redirected to the source site. Are you sure you want to
            leave this page?
          </DialogContentText>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleClose}>Disagree</Button>
          <Button href={redirectLink}>Agree</Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};
