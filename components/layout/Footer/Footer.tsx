import { FC } from 'react';
import styled from 'styled-components';
import { Container } from '../Container/Container';


const FooterStyled = styled.footer`
  background-color: var(--ui-bg);
`;
const FootContainer = styled.div`
  padding-bottom: 40px;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  border-top: 2px solid grey;
  @media (max-width: var(--mid-min)) {
    display: block;
    text-align: center;
  }
`;
const InfoWrapper = styled.div`
  display: flex;
  flex-direction: column;
  gap: 22px;
  @media (max-width: var(--mid-min)) {
    display: block;
  }
`;
const AdressWrapper = styled.div`
  display: flex;
  margin-top: 30px;
  @media (max-width: var(--mid-min)) {
    display: block;
  }
`;
const TradeMark = styled.div`
  color: var(--colors-text);
  line-height: 1.2;
  margin-top: 30px;
  @media (max-width: var(--mid-min)) {
    margin-top: 20px;
  }
`;

const Adress = styled.div`
  color: var(--colors-text);
  @media (max-width: var(--mid-min)) {
    margin-bottom: 10px;
  }
`;
const Rights = styled.span`
  text-transform: uppercase;
  font-weight: 700;
  font-size: 12px;
  margin-left: 47px;
  position: relative;
  text-decoration-line: none;
  color: #1976d2;
  @media (max-width: var(--mid-min)) {
    margin-top: 20px;
    margin-left: 0px;
  }
`;
const ContactsWrapper = styled.div`
  display: flex;
  align-items: center;
  @media (max-width: var(--mid-min)) {
    display: block;
    margin-top: 20px;
  }
`;
const Telephone = styled.div`
  color: var(--colors-text);
  font-weight: 700;
  line-height: 1.2;
  position: relative;
  margin: 0px 26px 0px 17px;
  @media (max-width: var(--mid-min)) {
    margin: 20px 0px;
  }
`;
export const Footer:FC = ()=> {
  return (
    <FooterStyled>
      <Container>
        <FootContainer>
          <InfoWrapper>
            <AdressWrapper>
              <Adress>18 South Westminster Drive Pacoima, CA 91331</Adress>
              <Rights>© StockApp. All Rights Reserved.</Rights>
            </AdressWrapper>
            <ContactsWrapper>
              <span className="footer__fb-link">
                <svg
                  className="footer__fb-img"
                  width="38"
                  height="38"
                  viewBox="0 0 38 38"
                  fill="none"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <path
                    className="footer__fb-img"
                    d="M38 19.1161C38 8.55857 29.4934 0 19 0C8.50658 0 0 8.55857 0 19.1161C0 28.6575 6.94803 36.5659 16.0312 38V24.6419H11.207V19.1161H16.0312V14.9046C16.0312 10.1136 18.8678 7.46723 23.2078 7.46723C25.2866 7.46723 27.4609 7.84059 27.4609 7.84059V12.545H25.0651C22.7048 12.545 21.9688 14.0185 21.9688 15.5302V19.1161H27.2383L26.3959 24.6419H21.9688V38C31.052 36.5659 38 28.6575 38 19.1161Z"
                    fill="#24A3FF"
                  />
                </svg>
              </span>
              <Telephone> (555) 555-1234</Telephone>
              <span className="footer__link footer__contact-link footer__link__hover">
                stockapp@gmai.com
              </span>
            </ContactsWrapper>
          </InfoWrapper>
          <TradeMark>© 2022 StockApp.com</TradeMark>
        </FootContainer>
      </Container>
    </FooterStyled>
  );
}
