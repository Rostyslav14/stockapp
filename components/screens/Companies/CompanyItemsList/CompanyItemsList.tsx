import { FC } from 'react';
import styled from 'styled-components';
import { ICompanyInfo } from 'helpers/types/others';
import { CompanyListItem } from '../CompanyListItem/CompanyListItem';
const List = styled.ul`
  display: grid;
  flex-direction: column;
  padding: 16px 2px;
  gap: 15px;
  border-radius: 8px;
`;

const TitleBlock = styled.div`
  border-radius: 8px;
  background-color: var(--colors-ui-base);
  color: var(--colors-bg);
  min-width: 986px;
  display: flex;
  padding: 6px 8px;
`;
const CategoryName = styled.span`
  font-weight: 600;
  text-transform: capitalize;
  font-size: 16px;
  text-align: right;
  width: 230px;
  flex: 230;
`;
const StartPlug = styled.div`
  width: 330px;
`;
const UnderButtonsPlug = styled.div`
  width: 290px;
`;
const IndustryIPOWrapper = styled.span`
  display: inline-flex;
  width: 180px;
  flex: 180;
`;

interface IProps {
  companiesList: ICompanyInfo[];
  onItemWebsiteClick: (link: string) => void;
}
export const CompnayItemsList: FC<IProps> = props => {
  const { companiesList, onItemWebsiteClick } = props;
  return (
    <List>
      <TitleBlock>
        <StartPlug />
        <IndustryIPOWrapper>
          <CategoryName>industry:</CategoryName>
          <CategoryName style={{ paddingRight: '18px' }}>ipo:</CategoryName>
        </IndustryIPOWrapper>
        <CategoryName>exchange:</CategoryName>
        <CategoryName>capitalization:</CategoryName>
        <UnderButtonsPlug />
      </TitleBlock>
      {companiesList.map(el => (
        <CompanyListItem
          onWebsiteClick={onItemWebsiteClick}
          key={el.logo}
          companyData={el}
        />
      ))}
    </List>
  );
};
