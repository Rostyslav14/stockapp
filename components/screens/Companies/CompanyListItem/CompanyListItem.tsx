import { FC } from 'react';
import Image from 'next/image';
import styled from 'styled-components';
import { ICompanyInfo } from 'helpers/types/others';
import { parseDate } from 'helpers/parsers/dateParsers';

const Item = styled.li`
  display: grid;
  grid-template-columns: 60px repeat(5, 16%);
  align-items: center;
  background-color: var(--ui-bg-additional);
  border-radius: 8px;
  padding: 6px 8px;
  justify-content: space-between;
  grid-template-rows: auto;
  transition: 0.6s filter;
  border: 3px solid transparent;

  &:hover {
    filter: brightness(120%);
    transition: 0.6s filter, 0.6s border;
    border: 3px solid #338033;
  }
`;
const LogoWrapper = styled.div`
  min-width: 60px;
  min-height: 60px;
  position: relative;
  border-radius: 8px;
`;
const Img = styled(Image)`
  border-radius: 8px;
`;
const Header = styled.h3`
  font-size: 16px;
  letter-spacing: 3px;
`;
const CompanyName = styled.div`
    color:var(--colors-ui-base)
    font-size:16px;
  `;
const GroupWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 10px;
  font-weight: 500;
  text-align: center;
  justify-content: space-around;
`;

const Capitalization = styled.span`
  color: var(--colors-ui-base);
  font-size: 16px;
`;
const IpoDate = styled.span``;
const ButtonsWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 8px;
`;

const Btn = styled.a`
  position: relative;
  color: #338033;
  text-align: center;
  text-decoration: none;
  padding: 6px 8px;
  border: 0;
  font-size: 18px;
  border-radius: 4px;
  font-family: 'Raleway', sans-serif;
  transition: 0.6s;
  overflow: hidden;
  color: var(--color-text);
  border: 1px solid #338033;
  &:focus {
    outline: 0;
  }
  &:before {
    content: '';
    display: block;
    position: absolute;
    background: rgba(255, 255, 255, 0.5);
    width: 80px;
    height: 100%;
    left: 0;
    top: 0;
    opacity: 0.5;
    filter: blur(30px);
    transform: translateX(-100px) skewX(-15deg);
  }
  &:after {
    content: '';
    display: block;
    position: absolute;
    background: rgba(255, 255, 255, 0.2);
    width: 30px;
    height: 100%;
    left: 30px;
    top: 0;
    opacity: 0;
    filter: blur(5px);
    transform: translateX(-100px) skewX(-15deg);
  }
  &:hover {
    background: #338033;
    cursor: pointer;
    color: var(--color-text);
    transition: 0.6s color;
    &:before {
      transform: translateX(300px) skewX(-15deg);
      opacity: 0.6;
      transition: 0.7s;
    }
    &:after {
      transform: translateX(300px) skewX(-15deg);
      opacity: 1;
      transition: 0.7s;
    }
  }
`;

const CurrencyItem = styled.span``;
const IndustryItem = styled.span``;

interface IProps {
  companyData: ICompanyInfo;
  onWebsiteClick: (link: string) => void;
}
function convertToInternationalCurrencySystem(labelValue: number) {
  // Six Zeroes for Trillions
  return Math.abs(Number(labelValue)) >= 1.0e6
    ? '$' + (Math.abs(Number(labelValue)) / 1.0e6).toFixed(2) + 'T'
    : // Three Zeroes for Billions
    Math.abs(Number(labelValue)) >= 1.0e3
    ? '$' + (Math.abs(Number(labelValue)) / 1.0e3).toFixed(2) + 'B'
    : // One Zeroes for Millions
    Math.abs(Number(labelValue)) >= 1.0
    ? '$' + (Math.abs(Number(labelValue)) / 1.0).toFixed(2) + 'M'
    : Math.abs(Number(labelValue));
}

export const CompanyListItem: FC<IProps> = props => {
  const { companyData: data, onWebsiteClick } = props;
  return (
    <Item>
      <LogoWrapper>
        <Img alt={data.country} fill src={data.logo} />
      </LogoWrapper>
      <GroupWrapper>
        <CompanyName>{data.name}</CompanyName>
        <Header>{data.ticker}</Header>
      </GroupWrapper>
      <GroupWrapper>
        <IndustryItem>{data.finnhubIndustry}</IndustryItem>
        <IndustryItem>{parseDate(data.ipo)}</IndustryItem>
      </GroupWrapper>
      <GroupWrapper>
        <CurrencyItem>{data.currency}</CurrencyItem>
        <CurrencyItem>{data.exchange}</CurrencyItem>
      </GroupWrapper>

      <GroupWrapper>
        <Capitalization>
          {convertToInternationalCurrencySystem(data.marketCapitalization)}
        </Capitalization>
      </GroupWrapper>
      <ButtonsWrapper>
        <Btn href={`/Indices/${data.ticker}`}>Statistics</Btn>
        <Btn onClick={() => onWebsiteClick(data.weburl)}>Website</Btn>
      </ButtonsWrapper>
    </Item>
  );
};
