import { FC, ReactNode } from 'react';
import styled from 'styled-components';
import InputLabel from '@mui/material/InputLabel';
import MenuItem from '@mui/material/MenuItem';
import FormControl from '@mui/material/FormControl';
import Select from '@mui/material/Select';
import { PEERS_CATEGORIES } from 'helpers/constants/constants';

const Wrapper = styled.div`
  margin-top: 40px;
  border: 1px solid var(--colors-ui-base);
  padding: 15px 20px;
  display: flex;
  flex-direction: column;
  gap: 15px;
`;

const Header = styled.h3`
  font-size: 30px;
  color: var(--colors-text);
  padding: 0;
  margin: 0px 0px 20px;
`;

interface IProps {
  onChangeSelect: (e: any) => void;
  children: ReactNode;
  value: string;
}

export const Peers: FC<IProps> = props => {
  const { onChangeSelect, children, value } = props;
  return (
    <Wrapper>
      <Header>Peers</Header>
      {children}
      <FormControl fullWidth>
        <InputLabel>Grouping</InputLabel>
        <Select
          value={value}
          label="Age"
          onChange={(e: any) => onChangeSelect(e)}
        >
          {PEERS_CATEGORIES.map(el => (
            <MenuItem className="peerSelectItem" key={el} value={el}>
              {el.split(/(?=[A-Z])/).join('-')}
            </MenuItem>
          ))}
        </Select>
      </FormControl>
    </Wrapper>
  );
};
