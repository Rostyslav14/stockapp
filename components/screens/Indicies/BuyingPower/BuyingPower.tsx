import { FC } from 'react';
import styled from 'styled-components';

interface IProps {}

const TopWrapper = styled.div`
  border-bottom: 1px solid var(--colors-ui-base);
  padding: 1.5rem 0;
  display: flex;
  justify-content: space-between;
  align-items: center;
`;
const BuyingHeader = styled.h3`
  margin: 0;
  color: var(--colors-text);
  font-size: 28px;
  text-transform: capitalize;
  font-weight: var(--fw-bold);
`;
const BuyingValue = styled.span`
  color: var(--colors-text);
  font-size: 28px;
  font-weight: var(--fw-bold);
`;
const BottomWrapper = styled.div`
  margin-top: 3rem;
  border: 1px solid var(--colors-ui-base);
  padding: 2rem 1.5rem;
  display: flex;
  flex-direction: column;
  gap: 1.2rem;
`;
const MarketStatus = styled.h5`
  color: var(--colors-ui-base);
  text-transform: uppercase;
  font-size: 16px;
  margin: 0;
`;
const MarketMessage = styled.span`
  color: var(--colors-text);
  font-size: 36px;
  text-transform: capitalize;
`;

export const BuyingPower: FC<IProps> = props => (
  <>
    <TopWrapper>
      <BuyingHeader>buying power</BuyingHeader>
      <BuyingValue>$4.11</BuyingValue>
    </TopWrapper>
    <BottomWrapper>
      <MarketStatus>market closed</MarketStatus>
      <MarketMessage>Happy Thanksgiving</MarketMessage>
    </BottomWrapper>
  </>
);
