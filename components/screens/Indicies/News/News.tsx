import { FC } from 'react';
import { BiNews } from 'react-icons/bi';
import { useRouter } from 'next/router';
import styled from 'styled-components';

const Wrapper = styled.div`
  margin: 3rem 0 1rem;
  background-color: var(--ui-bg);
  padding: 0.5rem 1rem;
  display: flex;
  flex-direction: column;
  gap: 1rem;
`;
const Header = styled.h3`
  margin: 0;
  font-size: 32px;
  color: var(--colors-text);
  text-transform: capitalize;
`;
const List = styled.ul`
  display: flex;
  gap: 1rem;
  flex-wrap: wrap;
  list-style-type: none;
`;
const ItemContainer = styled.li`
  cursor: pointer;
  color: var(--colors-text);
  border: 1px solid var(--colors-ui-base);
  border-radius: 24px;
  padding: 4px 8px;
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 4px;
  font-weight: var(--fw-normal);
  &:hover {
    color: var(--colors-link);
    border: 1px solid var(--colors-link);
    transition: color 0.9s, border 0.9s;
  }
`;
const Item = styled.span`
  letter-spacing: 1px;
  font-size: 16px;
  text-transform: capitalize;
`;

interface IProps {
  categories: string[];
  onItemClick: (itemName: string) => void;
}
export const News: FC<IProps> = props => {
  return (
    <Wrapper>
      <Header>News Categories</Header>
      <List>
        {[...props.categories].map(el => (
          <ItemContainer onClick={() => props.onItemClick(el)} key={el}>
            <BiNews size={26} />
            <Item>{el}</Item>
          </ItemContainer>
        ))}
      </List>
    </Wrapper>
  );
};
