import React from 'react';
import styled from 'styled-components';
import { Line } from 'react-chartjs-2';
import {
  Chart as ChartJS,
  Title,
  Tooltip,
  LineElement,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  Filler,
  TimeScale,
} from 'chart.js';
import { options } from './chart-config';

ChartJS.register(
  Title,
  Tooltip,
  LineElement,
  Legend,
  CategoryScale,
  LinearScale,
  PointElement,
  Filler,
  TimeScale
);

const Wrapper = styled.div`
  height: 100%;
  width: 100%;
  display: flex;
  align-items: center;
  justify-content: center;
`;

interface IProps {
  data: Array<{ x: string; y: number }>;
  name: string;
}

export const LineChart = (props: IProps)=> {
  return (
    <Wrapper>
      <Line
        data={{
          datasets: [
            {
              label: props.name,
              type: 'line',
              backgroundColor: 'black',
              borderColor: '#5AC53B',
              borderWidth: 2,
              pointBorderColor: 'rgba(0, 0, 0, 0)',
              pointBackgroundColor: 'rgba(0, 0, 0, 0)',
              pointHoverBackgroundColor: '#5AC53B',
              pointHoverBorderColor: '#000000',
              pointHoverBorderWidth: 4,
              pointHoverRadius: 6,
              data: props.data,
            },
          ],
        }}
        // @ts-ignore
        options={options}
      />
    </Wrapper>
  );
}
