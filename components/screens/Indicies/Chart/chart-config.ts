export const options = {
  options: {
    responsive: true,
    plugins: {
      tooltip: {
        mode: 'index',
        intersect: false,
      },
      title: {
        display: true,
        text: 'Chart.js Line Chart',
      },
    },
    hover: {
      mode: 'index',
      intersec: false,
    },
    scales: {
      x: {
        type: 'time',
        time: {
          unit: 'month',
        },
        title: {
          display: true,
          text: 'Month',
        },
      },
      y: {
        type: 'time',
        time: {
          unit: 'month',
        },
        title: {
          display: true,
          text: 'Value',
        },
        min: 0,
        max: 100,
        ticks: {
          // forces step size to be 50 units
          stepSize: 50,
        },
      },
    },
  },
};

export function getDatesInRange(startDate: any, endDate: any) {
  const date = new Date(startDate.getTime());

  date.setDate(date.getDate() + 1);

  const dates = [];

  while (date < endDate) {
    dates.push(new Date(date));
    date.setDate(date.getDate() + 1);
  }

  return dates;
}
