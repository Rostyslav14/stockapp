import { FC, ReactNode } from 'react';
import styled from 'styled-components';
import { ICryptoCurrency, ICurrencyInfo } from 'helpers/types/crypto';
import { TIME_CATEGORIES } from 'helpers/constants/constants';

interface IProps {
  children?: ReactNode;
  currency: ICryptoCurrency;
  handleTimeStampClick: (e: any, i: number) => void;
  activeIndex: number;
  info: ICurrencyInfo;
}
interface BtnProps {
  active?: any;
}

const Wrapper = styled.section`
  background-color: var(--colors-bg);
  width: 100%;
  height: 600px;
  transition: color 0.5s, background-color 0.5s;
`;
const GraphickWrapper = styled.div`
  position: relative;
  width: 100%;
  height: 100%;
  background-color: var(--ui-bg);
  transition: color 0.5s, background-color 0.5s;
`;
const InfoWrapper = styled.div`
  position: absolute;
  left: 125px;
  top: 0;
  display: flex;
  flex-direction: column;
  gap: 15px;
  width: 25%;
`;
const HeaderWrapper = styled.div`
  display: flex;
  gap: 8px;
`;
const CurrencyName = styled.h1`
  font-size: 32px;
  color: var(--colors-text);
  margin: 40px 0px 0px;
`;
const CurrencyCategory = styled.span`
  font-size: 16px;
  margin-left: 10px;
  color: var(--colors-ui-base);
`;
const CurrencyCourse = styled.strong`
  font-size: 32px;
  font-weight: var(--fw-bold);
  color: var(--colors-text);
`;
const PercentsWrapper = styled.div`
  display: flex;
  gap: 5px;
`;
const Percents = styled.span`
  color: var(--colors-text);
  font-size: 16px;
  font-weight: var(--fw-bold);
`;
const DayInfo = styled.span`
  color: --var(--color-ui-base);
  font-size: 14px;
`;
const Graph = styled.div`
  height: 94%;
  width: 100%;
`;

const TimeList = styled.ul`
  display: flex;
  gap: 12px;
  border-bottom: 1px solid var(--colors-ui-base);
  padding: 0 1rem;
`;
const TimeItem = styled.li`
  cursor: pointer;
  font-size: 16px;
  font-weight: var(--fw-bold);
  margin-bottom: -1px;
`;
const TimeBtn = styled.button<BtnProps>`
  color:${({ active }) =>
  active ? 'var(--color-green-main);' : 'var(--colors-text);'}
  font-size:16px;
  font-weight:var(--fw-bold);
  padding-bottom:6px;
  border-bottom:${({ active }) =>
  active ? '2px solid var(--color-green-main);' : 'none;'};`;

export const Graphick: FC<IProps> = props => (
  <Wrapper>
    <GraphickWrapper>
      <InfoWrapper>
        <HeaderWrapper>
          <CurrencyName>
            {props.info.ticker}
            <CurrencyCategory>©{props.info.name}</CurrencyCategory>
          </CurrencyName>
        </HeaderWrapper>
        <CurrencyCourse>{props.currency.c} USD</CurrencyCourse>
        <PercentsWrapper>
          {!!props.currency.dp && (
            <Percents>
              {props.currency.dp.toFixed(2)} (
              {(props.currency.c * (props.currency.dp / 100)).toFixed(2)})
            </Percents>
          )}
          <DayInfo>Today</DayInfo>
        </PercentsWrapper>
      </InfoWrapper>
      <Graph>{props.children}</Graph>
      <TimeList>
        {TIME_CATEGORIES.map((el, i) => (
          <TimeItem key={i}>
            <TimeBtn
              onClick={e => props.handleTimeStampClick(e, i)}
              active={i === props.activeIndex}
              key={i}
            >
              {el}
            </TimeBtn>
          </TimeItem>
        ))}
      </TimeList>
    </GraphickWrapper>
  </Wrapper>
);
