import { FC } from 'react';
import styled from 'styled-components';
import { ISearchItem } from 'helpers/types/crypto';
import { SearchItem } from '../SearchItem/SearchItem';

interface IProps {
  searchItems: ISearchItem[];
}
const List = styled.ul`
  display: flex;
  list-style-type: none;
  flex-direction: column;
  gap: 1rem;
`;
export const SearchItemsList: FC<IProps> = props => {
  const { searchItems } = props;
  return (
    <List>
      {searchItems.map(el => (
        <SearchItem item={el} key={el.symbol} />
      ))}
    </List>
  );
};
