import { FC, useEffect, useState } from 'react';
import ReactCountryFlag from 'react-country-flag';
import Link from 'next/link';
import styled, { css } from 'styled-components';
import { ISearchItem } from 'helpers/types/crypto';
import { parseCountryFromSymbol } from 'helpers/parsers/searchParsers';

const textHoverEff = css`
  background: linear-gradient(
      0deg,
      var(--color-yellow-main),
      var(--color-yellow-main)
    )
    no-repeat right bottom / 0 100%;
  transition: background-size 350ms, color 350ms;
  &:where(:hover, :focus-visible) {
    color: #000000;
    background-size: 100% 100%;
    background-position-x: left;
  }
`;
const Container = styled.li`
  cursor: pointer;
  background-color: var(--ui-bg);
  padding: 20px 16px;
  border-radius: 4px;
  transition: background-color 0.3s;
  &:hover {
    background-color: var(--ui-bg-accent);
    transition: background-color 0.3s;
  }
`;
const SLink = styled(Link)`
  width: 100%;
  text-decoration-line: none;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  justify-items: center;
`;
const LeftSideWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 12px;
  justify-self: start;
`;
const Header = styled.h4`
  text-transform: uppercase;
  font-weight: var(--fw-bold);
  color: var(--colors-text);
  letter-spacing: 1.5px;
  font-size: 20px;
  margin: 0;
  padding: 0;
  ${textHoverEff};
`;
const CompanyName = styled.span`
  text-align:center;
  text-transform: uppercase;
  font-weight: var(--fw-normal);
  color: var(--colors-text);
  letter-spacing: 0.5px;
  font-size: var(--fs-big);
  ${textHoverEff};
`;
const TypeInfo = styled.span`
  text-transform: uppercase;
  font-weight: var(--fw-light);
  font-size: var(--fs-sm);
  color: var(--colors-text);
  letter-spacing: 1px;
  ${textHoverEff};
`;
interface IProps {
  item: ISearchItem;
}

export const SearchItem: FC<IProps> = props => {
  const [country, setCountry] = useState<string>('');
  useEffect(() => {
    setCountry(parseCountryFromSymbol(props.item.symbol));
  }, []);

  return (
    <Container>
      <SLink href={`/Indices/${props.item.symbol}`}>
        <LeftSideWrapper>
          <ReactCountryFlag aria-label="United States" countryCode={country} />
          <Header>{props.item.symbol}</Header>
        </LeftSideWrapper>
        <CompanyName>{props.item.description}</CompanyName>
        <TypeInfo>{props.item.type || 'Common Stock'}</TypeInfo>
      </SLink>
    </Container>
  );
};
