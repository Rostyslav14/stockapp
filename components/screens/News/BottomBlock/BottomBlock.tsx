import { FC, ReactNode } from 'react';
import styled from 'styled-components';

interface IProps {
  children: ReactNode;
  isFullListShown: boolean;
  onClick: any;
}

const Wrapper = styled.div<{ isFullScreenShown: boolean }>`
    position: absolute;
    bottom: -15px;
    left: 0;
    right: 0;
    height: ${props => (props.isFullScreenShown ? '40px;' : '300px;')}
    text-transform:uppercase;
    z-index: 5;
    background: linear-gradient(180deg, rgba(245,245,250,0) 0%, var(--colors-ui-base) 86.5%);
    display:flex;
    align-items:center;
    justify-content:center;
    gap:5px;
    font-size:18px;
    letter-spacing:2px;
    color:var(--colors-bg);
    &:hover{
        color:var(--color-yellow-main);
        cursor:pointer;
    }
`;

export const BottomBlock: FC<IProps> = props => (
  <Wrapper onClick={props.onClick} isFullScreenShown={props.isFullListShown}>
    {props.children}
  </Wrapper>
);
