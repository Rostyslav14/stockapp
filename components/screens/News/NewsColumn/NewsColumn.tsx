import { FC } from 'react';
import styled from 'styled-components';
import { INews } from 'helpers/types/news';
import { NewsCard } from '@/components/shared/NewsCard/NewsCard';

interface IProps {
  categoryList:{name:string,list:INews[]}
  isFullList: boolean;
  onHeaderClick: (headerName: string) => void;
  isFullImg?: boolean;
  onNewsClick:(link:string)=>void;
}

const Column = styled.div<{ isFullList: boolean }>`
  width: 100%;
  height: ${props => (props.isFullList ? 'max-content' : '150rem')};
  overflow: hidden;
`;
const Header = styled.header<{ isFullList: boolean }>`
  font-size: ${props => (props.isFullList ? '38px' : '45px')};
  font-weight: ${props => (props.isFullList ? '600' : '800')};
  text-align: center;
  text-transform: capitalize;
  letter-spacing: ${props => (props.isFullList ? '4px' : '6px')};
  margin-bottom: 10px;
  border-bottom: 2px solid;
  &:hover {
    cursor: pointer;
    border-bottom: 2px solid var(--color-yellow-main);
    color: var(--color-yellow-main);
  }
`;

const NewsLine = styled.ul`
  display: grid;
  gap: 20px;
`;

export const NewsColumn: FC<IProps> = props => {
  const { categoryList, isFullList, isFullImg, onHeaderClick,onNewsClick } = props;
  return (
    <Column isFullList={isFullList}>
      <Header
        onClick={() => onHeaderClick(categoryList.name)}
        isFullList={isFullList}
      >
        {categoryList.name}:
      </Header>
      <NewsLine>
        {categoryList.list?.map(news => (
          <NewsCard onClick={onNewsClick} isFullImg={isFullImg} key={news.id} news={news} />
        ))}
      </NewsLine>
    </Column>
  );
};
