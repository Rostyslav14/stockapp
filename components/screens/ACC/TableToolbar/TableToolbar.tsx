import { FC } from 'react';
import Toolbar from '@mui/material/Toolbar';
import IconButton from '@mui/material/IconButton';
import Tooltip from '@mui/material/Tooltip';
import FilterListIcon from '@mui/icons-material/FilterList';

interface IProps {
  numSelected: number;
}

export const TableToolbar: FC<IProps> = props => (
  <Toolbar
    sx={{
      pl: { sm: 2 },
      pr: { xs: 1, sm: 1 },
    }}
  >
    <Tooltip title="Filter list">
      <IconButton>
        <FilterListIcon />
      </IconButton>
    </Tooltip>
  </Toolbar>
);
