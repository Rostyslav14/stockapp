import { FC } from 'react';
import {
  AiFillFacebook,
  AiFillLinkedin,
  AiFillTwitterSquare,
} from 'react-icons/ai';
import Link from 'next/link';
import styled, { css } from 'styled-components';
import { VscLinkExternal } from 'react-icons/vsc'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  text-align: center;
`;
const Header = styled.h1`
  font-size: 38px;
  letter-spacing: 2px;
  font-weight: 1000;
`;
const IconsContainer = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  gap: 10px;
  margin: 5px 0;
`;
const IconWrapper = styled(Link)`
  padding: 5px;
  border: 1px solid var(--shadow-ui-border);
`;

const IconsCSS = css`
  cursor: pointer;
  height: 40px;
  width: 40px;
  transition: background-color 0.2s;
  &:hover {
    border-radius: 2px;
    background-color: var(--color-yellow-main); // <Thing> when hovered
    transition: background-color 0.2s;
  }
`;
const Descript = styled.div`
  width: 650px;
  font-size: var(--fs-big);
  font-weight: 400;
`;
const FB = styled(AiFillFacebook)`
  ${IconsCSS}
`;
const LIN = styled(AiFillLinkedin)`
  ${IconsCSS}
`;
const TW = styled(AiFillTwitterSquare)`
  ${IconsCSS}
`;
const ArchiveLink = styled.span`
  cursor:pointer;
  color: var(--colors-link-hv);
  text-decoration-line: none;
  border-bottom: 1px solid var(--colors-link-hv);
  &:hover {
    filter: brightness(115%); /* Completely black */
  }
`;
interface IProps {
  handleLinkClick: (link: string) => void;
}

export const Description: FC<IProps> = props => {
  const { handleLinkClick } = props;

  return (
    <Wrapper>
      <Header>Advisory Committee Calendar</Header>
      <Descript>
        This page contains notices of advisory committee meetings. For previous
        years advisory committee calendars, see the
        <> </>
        <ArchiveLink
          onClick={() =>
            handleLinkClick(
              'http://wayback.archive-it.org/7993/20170110233952/'
            )
          }
        >
          FDA ArchiveExternal. <VscLinkExternal/>
        </ArchiveLink>
      </Descript>
      <IconsContainer>
        <IconWrapper href="https://www.facebook.com/sharer/sharer.php?u=https://www.fda.gov%2Fadvisory-committees%2Fadvisory-committee-calendar">
          <FB style={{ color: '#5e77aa' }} />
        </IconWrapper>
        <IconWrapper href="https://www.linkedin.com/shareArticle?mini=true&url=https://www.fda.gov%2Fadvisory-committees%2Fadvisory-committee-calendar&title=Advisory%20Committee%20Calendar&source=FDA">
          <LIN style={{ color: '#007bb6' }} />
        </IconWrapper>
        <IconWrapper href="https://twitter.com/intent/tweet/?text=Advisory%20Committee%20Calendar&url=https://www.fda.gov%2Fadvisory-committees%2Fadvisory-committee-calendar">
          <TW style={{ color: '#78ddff' }} />
        </IconWrapper>
      </IconsContainer>
    </Wrapper>
  );
};
