import { FC } from 'react';
import Box from '@mui/material/Box';
import Table from '@mui/material/Table';
import TableBody from '@mui/material/TableBody';
import TableCell from '@mui/material/TableCell';
import TableContainer from '@mui/material/TableContainer';
import TablePagination from '@mui/material/TablePagination';
import TableRow from '@mui/material/TableRow';
import Paper from '@mui/material/Paper';
import { TOrder, ITableData } from 'helpers/types/others';
import { EnhancedTableHead } from './TableHeader/TableHeader';

interface IProps {
  order: TOrder;
  orderBy: 'fromDate' | 'toDate';
  page: number;
  rowsPerPage: number;
  handleRequestSort: (
    event: React.MouseEvent<unknown>,
    property: 'fromDate' | 'toDate'
  ) => void;
  //   handleChangeRowsPerPage: any;
  handleChangePage: any;
  rows: ITableData[];
}

export const ACCTable: FC<IProps> = props => {
  const {
    order,
    orderBy,
    page,
    rowsPerPage,
    handleRequestSort,
    // handleChangeRowsPerPage,
    handleChangePage,
    rows,
  } = props;

  // Avoid a layout jump when reaching the last page with empty rows.
  const emptyRows =
    page > 0 ? Math.max(0, (1 + page) * rowsPerPage - rows.length) : 0;
  return (
    <Box sx={{ width: '100%' }}>
      <Paper sx={{ width: '100%', mb: 2 }}>
        <TableContainer>
          <Table
            sx={{ minWidth: 750 }}
            aria-labelledby="tableTitle"
            size="medium"
          >
            <EnhancedTableHead
              order={order}
              orderBy={orderBy}
              onRequestSort={handleRequestSort}
              rowCount={rows.length}
            />
            <TableBody>
              {rows
                .slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage)
                .map((row: ITableData, index: any) => (
                  <TableRow hover role="checkbox" tabIndex={-1} key={row.url}>
                    <TableCell align="center">{row.fromDate}</TableCell>
                    <TableCell align="center">{row.toDate}</TableCell>
                    <TableCell align="center">{row.eventDescription}</TableCell>
                    <TableCell align="center">{row.url}</TableCell>
                  </TableRow>
                ))}
              {emptyRows > 0 && (
                <TableRow
                  style={{
                    height: 53 * emptyRows,
                  }}
                >
                  <TableCell colSpan={6} />
                </TableRow>
              )}
            </TableBody>
          </Table>
        </TableContainer>
        <TablePagination
          //   rowsPerPageOptions={[5, 10, 25]}

          component="div"
          count={rows.length}
          rowsPerPage={20}
          page={page}
          onPageChange={handleChangePage}
          labelRowsPerPage={false}
          rowsPerPageOptions={[]}
          //   onRowsPerPageChange={handleChangeRowsPerPage}
        />
      </Paper>
    </Box>
  );
};
