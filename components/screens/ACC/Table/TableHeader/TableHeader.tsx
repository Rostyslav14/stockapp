import { FC } from 'react';
import Box from '@mui/material/Box';
import TableCell from '@mui/material/TableCell';
import TableHead from '@mui/material/TableHead';
import TableRow from '@mui/material/TableRow';
import TableSortLabel from '@mui/material/TableSortLabel';
import { visuallyHidden } from '@mui/utils';
import { ITableData, TOrder } from 'helpers/types/others';

interface HeadCell {
  disablePadding: boolean;
  id: keyof ITableData;
  label: string;
  numeric: boolean;
}

const headCells: readonly HeadCell[] = [
  {
    id: 'fromDate',
    numeric: false,
    disablePadding: true,
    label: 'Start Date',
  },
  {
    id: 'toDate',
    numeric: true,
    disablePadding: false,
    label: 'End Date',
  },
  {
    id: 'eventDescription',
    numeric: false,
    disablePadding: false,
    label: 'Event Description',
  },
  {
    id: 'url',
    numeric: false,
    disablePadding: false,
    label: 'URL',
  },
];

interface IProps {
  rowCount: number;
  onRequestSort: any;
  order: TOrder;
  orderBy: 'fromDate' | 'toDate';
}
export const EnhancedTableHead: FC<IProps> = props => {
  const { order, orderBy, rowCount, onRequestSort } = props;
  const createSortHandler =
    (property: keyof ITableData) => (event: React.MouseEvent<unknown>) => {
      onRequestSort(event, property);
    };

  return (
    <TableHead>
      <TableRow>
        {headCells.map(headCell => {
          if (headCell.id === 'fromDate' || headCell.id === 'toDate') {
            return (
              <TableCell
                key={headCell.id}
                align="center"
                sortDirection={orderBy === headCell.id ? order : false}
              >
                <TableSortLabel
                  style={{ whiteSpace: 'nowrap' }}
                  active={orderBy === headCell.id}
                  direction={orderBy === headCell.id ? order : 'asc'}
                  onClick={createSortHandler(headCell.id)}
                >
                  {headCell.label}
                  {orderBy === headCell.id ? (
                    <Box component="span" sx={visuallyHidden}>
                      {order === 'desc'
                        ? 'sorted descending'
                        : 'sorted ascending'}
                    </Box>
                  ) : null}
                </TableSortLabel>
              </TableCell>
            );
          }
          return (
            <TableCell key={headCell.id} align="center" sortDirection={false}>
              <div style={{ whiteSpace: 'nowrap' }}>{headCell.label}</div>
            </TableCell>
          );
        })}
      </TableRow>
    </TableHead>
  );
};
