import { FC } from 'react';
import styled from 'styled-components';
import { NewsCard } from 'components/shared/NewsCard/NewsCard';
import { INews } from 'helpers/types/news';

interface IProps {
  news: INews[];
  onNewsClick: (e: any) => void;
}
const List = styled.ul`
  margin-top: 40px;
  display: grid;
  grid-template-columns: 1fr 1fr 1fr;
  gap: 20px;
`;
const NewsCardList: FC<IProps> = props => (
  <List>
    {props.news.map(el => (
      <NewsCard onClick={props.onNewsClick} news={el} key={el.id} />
    ))}
  </List>
);
export default NewsCardList;
