import { FC } from 'react';
import styled from 'styled-components';
import { Input } from 'antd';

const { Search } = Input;

const Wrapper = styled.div`
  margin: 25px 0px;
  width: 100%;
`;
interface IProps {
  onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
  value: string;
  [x: string]: any;
}
export const SearchBar: FC<IProps> = props => {
  const { attributes, onChange } = props;
  return (
    <Wrapper>
      <Search
        autoFocus
        onChange={e => onChange(e)}
        placeholder="Search..."
        size="large"
        value={props.value}
        enterButton
        {...attributes}
      />
    </Wrapper>
  );
};
