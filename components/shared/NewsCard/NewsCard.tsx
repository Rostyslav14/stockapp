import { FC } from 'react';
import Link from 'next/link';
import styled from 'styled-components';
import { INews } from 'helpers/types/news';

const ImgWrapper = styled.div<{ img: string; isFullImg?: boolean }>`
  cursor: pointer;
  background-image: url(${props => props.img});
  background-size: ${props => (props.isFullImg ? 'cover' : 'contain')};
  border-radius: 0.5rem;
  grid-row: 1 / 3;
  grid-column: 2;
  position: absolute;
  height: ${props => (props.isFullImg ? 'max-height' : '200px')};
  left: 0;
  right: 0;
  bottom: 0;
  top: 0;
  transition: color 0.25s;
`;
const Article = styled.li`
  border: 2px solid transparent;
  list-style-type: none;
  background-color: var(--ui-bg);
  box-shadow: var(--shadow-ui-border);
  border-radius: 8px;
  transition: all 0.5s;
  text-shadow: 3px 3px 11px transparent -2px 1px 15px transparent;
  min-height: 435px;
  &:hover ${ImgWrapper} {
    background-size: cover;
    background-repeat: repeat;
    height: 100% !important;
    transition: all 0.5s;
  }
  &:hover {
    border: 2px solid var(--colors-link-hv);
    text-shadow: 3px 3px 11px var(--color-yellow-main);
    transition: all 0.5s;
  }
`;
const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;
  height: 100%;
`;

const Body = styled.div`
  text-align: center;
  margin-top: 190px;
  z-index: 1;
  padding: 0 4px;
  height: 100%;
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  padding-bottom: 5px;
`;

const Category = styled.div`
  font-weight: var(--fw-bold);
  text-transform: uppercase;
  font-size: 14px;
  letter-spacing: 2px;
`;

const Description = styled.p`
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 3;
  -webkit-box-orient: vertical;
`;
const Header = styled.h3`
  overflow: hidden;
  display: -webkit-box;
  -webkit-line-clamp: 2;
  -webkit-box-orient: vertical;
`;
const BodyBottomContainer = styled.div`
  height: calc(100% - 100px);
  display: flex;
  flex-direction: column;
  justify-content: space-between;
`;
interface IProps {
  news: INews;
  isFullImg?: boolean;
  onClick: (link: string) => void;
}
export const NewsCard: FC<IProps> = props => {
  const { news, isFullImg, onClick } = props;
  return (
    <Article onClick={() => onClick(news.url)}>
      <Wrapper>
        <ImgWrapper isFullImg={isFullImg} img={news.image}></ImgWrapper>
        <Body>
          <Header>{news.headline}</Header>
          <BodyBottomContainer>
            <Category>{news.category}</Category>
            <Description>{news.summary}</Description>
          </BodyBottomContainer>
        </Body>
      </Wrapper>
    </Article>
  );
};
