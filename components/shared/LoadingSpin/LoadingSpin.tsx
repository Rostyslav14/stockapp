import { FC } from 'react';
import styled from 'styled-components';
import { Spin } from 'antd';

const Container = styled.div`
  position: absolute;
  z-index: 999;
  left: 0;
  top: 0;
  bottom: 0;
  height: 100vh;
  width: 100vw;
  display: grid;
  place-content: center;
`;
export const LoadingSpin: FC = () => {
  return (
    <Container>
      <Spin />
    </Container>
  );
};
