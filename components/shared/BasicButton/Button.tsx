import styled from 'styled-components';

const ButtonStyled = styled.button`
  padding: 13px 35px;
  border: none;
  border-radius: 12px;
  background-color: var(--color-yellow-main);
  -webkit-box-shadow: 0px 0px 20px var(--colors-ui-base);
  box-shadow: 0px 0px 20px var(--colors-ui-base);
  font-size: 15px;
  font-weight: 700;
  color: black;
  cursor: pointer;
  transition: filter 0.5s;

  &:hover {
    filter: contrast(120%);
    transition: filter 0.5s;
    color: black;
  }
`;
interface IProps {}

export const Button: React.FC<
  IProps & React.ComponentProps<'button'>
> = props => <ButtonStyled {...props} />;
