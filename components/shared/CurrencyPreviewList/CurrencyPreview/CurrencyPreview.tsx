import { FC } from 'react';
import { CgChevronDown, CgChevronUp } from 'react-icons/cg';
import Link from 'next/link';
import styled from 'styled-components';
import { ICryptoCurrency } from 'helpers/types/crypto';

const Wrapper = styled.li`
  height: 100%;
  width: 100%;
`;
const Li = styled(Link)`
  margin-top: 10px;
  border-right: 1px solid var(--colors-ui-base);
  display: flex;
  padding: 0 8px;
  flex-direction: column;
  text-decoration-line: none;
  &:hover {
    background-color: var(--ui-bg);
    transition: background-color 0.6s;
  }
`;

const TopWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  gap: 25px;
`;
const CurrencyName = styled.span`
  text-transform: uppercase;
  color: var(--colors-text);
  font-size: var(--fs-sm);
  font-weight: var(--fw-normal);
`;
const CurrencyValue = styled.span`
  color: var(--colors-text);
  font-size: var(--fs-sm);
  font-weight: var(--fw-bold);
`;
const BottomWrapper = styled.div`
  display: flex;
  align-items: center;
  gap: 5px;
`;
const Percents = styled.span`
  color: var(--color-green-main);
  font-size: var(--fs-biggest);
  font-weight: var(--fw-bold);
`;
const Increase = styled.span`
  color: var(--color-green-main);
  font-size: var(--fs-md);
`;
const Decrease = styled(Increase)`
  color: var(--color-red-main);
`;

const CustomUpChevron = styled(CgChevronUp)`
  margin: auto;
  color: var(--color-green-main);
  width: 25px;
  height: 25px;
  font-weight: var(--fw-normal);
`;
const CustomDownChevron = styled(CgChevronDown)`
  margin: auto;
  color: var(--color-red-main);
  width: 25px;
  height: 25px;
  font-weight: var(--fw-normal);
`;
interface IProps {
  currency: ICryptoCurrency;
  index: number;
}
export const CurrencyPreview: FC<IProps> = props => (
  <Wrapper>
    <Li href={`/Indices/${props.currency.name}`}>
      <TopWrapper>
        <CurrencyName>{props.currency.name}</CurrencyName>
        <CurrencyValue>{props.currency.c}</CurrencyValue>
      </TopWrapper>
      <BottomWrapper>
        {props.currency.dp < 1 && (
          <>
            {' '}
            <div>
              <CustomDownChevron />
            </div>
            <Percents />
            <Decrease>
              {(props.currency.c * (props.currency.dp / 100)).toFixed(3)}
            </Decrease>
          </>
        )}
        {props.currency.dp >= 1 && (
          <>
            {' '}
            <div>
              <CustomUpChevron />
            </div>
            <Percents />
            <Increase>
              {(props.currency.c * (props.currency.dp / 100)).toFixed(3)}
            </Increase>
          </>
        )}
      </BottomWrapper>
    </Li>
  </Wrapper>
);
