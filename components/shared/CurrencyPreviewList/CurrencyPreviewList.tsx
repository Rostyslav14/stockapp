import { FC } from 'react';
import styled from 'styled-components';
import { ICryptoCurrency } from 'helpers/types/crypto';
import { CurrencyPreview } from './CurrencyPreview/CurrencyPreview';

const StyledList = styled.ul`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  grid-auto-flow: column;
  width: 100%;
`;
interface IProps {
  currencyList: ICryptoCurrency[];
}

export const CurrencyPreviewList: FC<IProps> = props => (
  <StyledList>
    {props.currencyList.map((el, i) => (
      <CurrencyPreview key={el.name ? el.name : el.c} currency={el} index={i} />
    ))}
  </StyledList>
);
