import React, { Component, ErrorInfo, ReactNode } from 'react';
import { withRouter } from 'next/router';
import styled from 'styled-components';
import Link from 'next/link';
import { Button } from 'components/shared/BasicButton/Button';

const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100vh;
  flex-direction: column;
`;
const Container = styled.div`
  max-width: 600px;
  padding-bottom:200px;
  display:flex;
  flex-direction:column;
  align-items:center;
  justify-content:center;
  text-align:center;
`;
const StyledLink = styled(Link)`
    color:var(--colors-link);
    &:hover{
        color:var(--colors-link-hv)
    }
`

interface Props {
  router: any;
}

interface State {

}

class ErrorPlug extends Component<Props, State> {
  constructor(props : Props) {
    super(props)
    this.state = {}
  }



  public render() {
      return (
        <Wrapper>
          <Container>
            <h2>
              Sorry, you did't have acces to this data. Only users who buy{' '}
              <StyledLink href={'https://finnhub.io/pricing'}>
                finnhub subscription
              </StyledLink>{' '}
              can get acces to this page :c
            </h2>
            <Button onClick={()=>this.props.router.back()}>Go Back</Button>
          </Container>
        </Wrapper>
      );
    }
}

export default withRouter(ErrorPlug);
