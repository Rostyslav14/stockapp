import { ICryptoCurrency } from 'helpers/types/crypto';

export const currencyActions = {
  GET_PREVIEW_ITEMS_SUCCES: '[CURRENCY] GET PREVIEW ITEMS',
  GET_PREVIEW_ITEMS_ERROR: '[CURRENCY] ERROR GET PREVIEW ITEMS',
};

export const loadSomethingSucces = (data: ICryptoCurrency[]) => ({
  type: currencyActions.GET_PREVIEW_ITEMS_SUCCES,
  payload: { data },
});

export const loadSomethingError = (error: any) => ({
  type: currencyActions.GET_PREVIEW_ITEMS_ERROR,
  payload: { error },
});
