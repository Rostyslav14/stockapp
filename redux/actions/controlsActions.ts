export const controlsActionsDeclarations = {
  SHOW_LOADER: '[LOADER] SHOW',
  HIDE_LOADER: '[LOADER] HIDE',
  ACTIVATE_REDIRECTDIALOG: '[REDIRECTDIALOG] ACTIVATE',
  DEACTIVATE_REDIRECTDIALOG: '[REDIRECTDIALOG] DEACTIVATE',
};

const showLoader = () => ({
  type: controlsActionsDeclarations.SHOW_LOADER,
  payload: {},
});

const hideLoader = () => ({
  type: controlsActionsDeclarations.HIDE_LOADER,
  payload: {},
});

const activateRedirectDialog = (redirectLink:string) => ({
  type: controlsActionsDeclarations.ACTIVATE_REDIRECTDIALOG,
  payload: {redirectLink},
});

const deactivateRedirectDialog = () => ({
  type: controlsActionsDeclarations.DEACTIVATE_REDIRECTDIALOG,
  payload: {},
});

export const controlsActions = {
  showLoader,
  hideLoader,
  activateRedirectDialog,
  deactivateRedirectDialog,
};
