import { ICryptoCurrency } from 'helpers/types/crypto';
import { currencyActions } from '../actions/currencyActions';

export interface ICurrencyState {
  cryptoPreviewItems: ICryptoCurrency[];
  error: Error | null;
}

const initialState: ICurrencyState = {
  cryptoPreviewItems: [],
  error: null,
};

export const CurrencyReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case currencyActions.GET_PREVIEW_ITEMS_SUCCES:
      return {
        ...state,
        cryptoPreviewItems: action.payload.data,
      };
    case currencyActions.GET_PREVIEW_ITEMS_ERROR:
      return {
        ...state,
        error: action.payload.error,
      };
    default:
      return state;
  }
};
