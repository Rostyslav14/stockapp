import { combineReducers } from 'redux';
import { ControlsReducer, IControlsState } from './controlsReducer';
import { CurrencyReducer, ICurrencyState } from './currencyReducer';

export interface IRootState {
  currency: ICurrencyState;
  controls: IControlsState;
}

export const rootReducer = combineReducers({
  currency: CurrencyReducer,
  controls: ControlsReducer,
});
