import { controlsActionsDeclarations } from 'redux/actions/controlsActions';

export interface IControlsState {
  isLoaderShown: boolean;
  isRedirectDialogShown: boolean;
  redirectLink: string;
}

const initialState: IControlsState = {
  isLoaderShown: false,
  isRedirectDialogShown: false,
  redirectLink: '',
};

export const ControlsReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case controlsActionsDeclarations.SHOW_LOADER:
      return {
        ...state,
        isLoaderShown: true,
      };
    case controlsActionsDeclarations.HIDE_LOADER:
      return {
        ...state,
        isLoaderShown: false,
      };
    case controlsActionsDeclarations.ACTIVATE_REDIRECTDIALOG:
      return {
        ...state,
        isRedirectDialogShown: true,
        redirectLink: action.payload.redirectLink,
      };
    case controlsActionsDeclarations.DEACTIVATE_REDIRECTDIALOG:
      return {
        ...state,
        isRedirectDialogShown: false,
        redirectLink: '',
      };
    default:
      return state;
  }
};
