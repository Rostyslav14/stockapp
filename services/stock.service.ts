import { rootHttpRequest } from './root.service';

export const stockService = {
  getCurrencyData,
  getStockData,
  getChartData,
  searchBySymbol,
  getSimilarPeerNames,
  getCompanyInfo,
  getACCData,
};

export async function getCurrencyData(curr: string) {
  const reqQuery = {
    params: {
      symbol: curr,
    },
  };

  const data = await rootHttpRequest.get('quote?', reqQuery);
  return data;
}

export async function getStockData(stock: string) {
  const reqQuery = {
    params: {
      symbol: stock,
    },
  };
  const data = await rootHttpRequest.get('/stock/profile2?', reqQuery);
  return data;
}

export async function getChartData(
  symbol: string,
  resolution: string,
  from: string,
  to: string,
) {
  const reqQuery = {
    params: {
      symbol,
      resolution,
      from,
      to,
    },
  };

  const data = await rootHttpRequest.get(
    'https://finnhub.io/api/v1/stock/candle?',
    reqQuery,
  );
  return data;
}

export async function searchBySymbol(query: string) {
  const reqQuery = {
    params: {
      q: query,
    },
  };
  const data = await rootHttpRequest.get('/search?', reqQuery);
  return data;
}

export async function getSimilarPeerNames(symbol: string, grouping: string) {
  const reqQuery = {
    params: {
      symbol,
      grouping,
    },
  };
  const data = await rootHttpRequest('/stock/peers?', reqQuery);
  return data;
}

export async function getCompanyInfo(symbol: string) {
  const reqQuery = {
    params: {
      symbol,
    },
  };
  const data = await rootHttpRequest('/stock/profile2?', reqQuery);
  return data;
}

export async function getACCData() {
  const reqQuery = {};
  const data = await rootHttpRequest('/fda-advisory-committee-calendar');
  return data;
}
