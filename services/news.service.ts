import { rootHttpRequest } from './root.service';

export const newsService = {
  getNews,
};

export async function getNews(category = 'general') {
  const reqQuery = {
    params: {
      category,
    },
  };
  const data = await rootHttpRequest.get('/news?', reqQuery);
  return data;
}
